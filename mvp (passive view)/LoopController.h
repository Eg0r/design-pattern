#pragma once

#include <vector>

#include "view/ViewEngine.h"

class LoopController
{
public:
	LoopController();
	void run();
	void addViewEngine(ViewEngine*);
private:
	std::vector<ViewEngine*> _viewEngines;
};

