#include "LoopController.h"

LoopController::LoopController()
{

}

void LoopController::run()
{
	int size;
	while (1)
	{
		size = _viewEngines.size();
		for (int i = 0; i < size; i++)
		{
			_viewEngines[i]->run();
		}
	}
}

void LoopController::addViewEngine(ViewEngine* viewEngine)
{
	_viewEngines.push_back(viewEngine);
}