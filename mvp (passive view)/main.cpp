﻿#include "./model/DataModel.h"
#include "./view/console/ConsoleSimpleView.h"
#include "./presenter/DataPresenter.h"

#include "./view/gui/WindowView.h"
#include "./view/ViewEngine.h"
#include "LoopController.h"

#include <windows.h>



int main()
{
	DataModel* dataModel = new DataModel();
	DataPresenter* dataPresenter = new DataPresenter(dataModel);

	ConsoleSimpleView* view = new ConsoleSimpleView(dataPresenter);
	ViewEngine* viewEngine = view->getEngine();

	WindowView* view2 = new WindowView(dataPresenter);
	ViewEngine* viewEngine2 = view2->getEngine();

	LoopController* loopController = new LoopController();
	loopController->addViewEngine(viewEngine);
	loopController->addViewEngine(viewEngine2);
	loopController->run();

	return 0;
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	DataModel* dataModel = new DataModel();
	DataPresenter* dataPresenter = new DataPresenter(dataModel);
	//ConsoleSimpleView* view = new ConsoleSimpleView(dataPresenter);
	
	WindowView* view = new WindowView(dataPresenter);

	ViewEngine* viewEngine = view->getEngine();
	viewEngine->run();
	

	return 0;
}

