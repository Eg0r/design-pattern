#pragma once

#include "../model/DataModel.h"
#include "../Observer.h"

class DataPresenter: public Observer, public Observable
{
public:
	DataPresenter(DataModel* model);
	void setData(int data);
	int getData();
	int getResultData();
	void update();
private:
	DataModel* _model;
};

