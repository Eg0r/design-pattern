#include "DataPresenter.h"

#include <stdio.h>

DataPresenter::DataPresenter(DataModel* model)
{
	_model = model;
	_model->addObserver(this);
}

void DataPresenter::setData(int data)
{
	_model->setData(data);
}

int DataPresenter::getData()
{
	return _model->getData();
}

int DataPresenter::getResultData()
{
	return _model->getResultData();
}

void DataPresenter::update()
{
	notifyUpdate();
}
