#pragma once

#include <stdio.h>

#include "../ViewEngine.h"

class ConsoleView;

class ViewConsoleEngine: public ViewEngine
{
public:
	ViewConsoleEngine(ConsoleView*);
	void run();
private:
	HANDLE _hstd;
	INPUT_RECORD _IR;
	DWORD _read;
	int _bufSize;
	int _bufCounter;
	WCHAR* _buf;
	WCHAR _key;

	ConsoleView* _view;
};

