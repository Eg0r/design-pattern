#pragma once

#include "../../model/DataModel.h"
#include "ConsoleView.h"

class ConsoleSimpleView: public ConsoleView
{
public:
	ConsoleSimpleView(DataPresenter*);
	void update();
	void dispatch(WCHAR*);
	void print(DataPresenter*);
	void printWaitData();
	void printError();
private:
	DataPresenter* _presenter;
};

