#include "ConsoleSimpleView.h"

ConsoleSimpleView::ConsoleSimpleView(DataPresenter* presenter)
{
	_presenter = presenter;
	_presenter->addObserver(this);

	printWaitData();
}

void ConsoleSimpleView::update()
{
	print(_presenter);
}

void ConsoleSimpleView::dispatch(WCHAR* _buf)
{
	int data;
	int scanfResult = swscanf_s(_buf, L"%d", &data);
	if (scanfResult != 0)
	{
		_presenter->setData(data);
	}
	else
	{
		printError();
		//removeError();
	}
	printWaitData();
}

void ConsoleSimpleView::print(DataPresenter* presenter)
{
	printf("data: %d; result: %d\r\n\r\n", presenter->getData(), presenter->getResultData());
}

void ConsoleSimpleView::printWaitData()
{
	printf("enter digit for power of 2\r\n");
}

void ConsoleSimpleView::printError()
{
	printf("entered data not corrected\r\n\r\n");
}