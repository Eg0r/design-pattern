#pragma once

#include "../../presenter/DataPresenter.h"
#include "../../Observer.h"
#include "../View.h"
#include "../console/ViewConsoleEngine.h"

#include <stdio.h>
#include <ctype.h>


class ConsoleView : public View
{
public:
	ConsoleView();
	void removeError();
	ViewEngine* getEngine();
	virtual void update() = 0;
	virtual void dispatch(WCHAR*) = 0;
};

