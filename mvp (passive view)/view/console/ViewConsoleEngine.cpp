#include "ViewConsoleEngine.h"
#include "ConsoleView.h"

ViewConsoleEngine::ViewConsoleEngine(ConsoleView* view)
{
	_hstd = GetStdHandle(STD_INPUT_HANDLE);
	_bufSize = 30;
	_bufCounter = 0;
	_buf = new WCHAR[_bufSize];

	_view = view;
}

void ViewConsoleEngine::run()
{
	PeekConsoleInputA(_hstd, &_IR, 1, &_read);
	if (_read > 0)
	{
		ReadConsoleInputA(_hstd, &_IR, 1, &_read);
		if ((_IR.EventType == KEY_EVENT) && (_IR.Event.KeyEvent.bKeyDown == 1))
		{
			_key = _IR.Event.KeyEvent.uChar.UnicodeChar;
			if (_key != 0)
			{
				wprintf(&_key);
				_buf[_bufCounter] = _key;
				_bufCounter++;
				if (_key == '\r')
				{
					printf("\n");
					_buf[_bufCounter] = '\n';
					_view->dispatch(_buf);

					for (int i = 0; i < _bufCounter; i++)
					{
						_buf[i] = 0;
					}
					_bufCounter = 0;
				}
			}
		}
		//IR[i].EventType = NULL;
		//FlushConsoleInputBuffer(hstd);
	}
}
