#pragma once

#include <windows.h>

class View;

class ViewEngine
{
public:
	virtual void run() = 0;
};

