#pragma once

#include "../Observer.h"

class ViewEngine;

class View: public Observer
{
public:
	virtual ViewEngine* getEngine() = 0;
};

