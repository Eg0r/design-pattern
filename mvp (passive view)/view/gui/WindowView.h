#pragma once

#include "../View.h"

#include "../../resource.h"

#define IDS_APP_TITLE			103
#define IDI_WINDOWSPROJECT1			107
#define IDI_SMALL				108


#include <windows.h>
#include <tchar.h>
#include <string.h>

#include "../../presenter/DataPresenter.h"

static TCHAR szWindowClass[] = _T("win32app");
static TCHAR szTitle[] = _T("WindowView");

class ViewEngine;

class WindowView: public View
{
public:
	WindowView(DataPresenter*);
	static BOOL CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
	static INT_PTR CALLBACK About(HWND, UINT, WPARAM, LPARAM);

	ViewEngine* getEngine();

	void update();
private:
	HACCEL loadAccel();
	HINSTANCE _hInstance;
	HWND _hWnd;

	DataPresenter* _presenter;
};

