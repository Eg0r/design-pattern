#include "WindowView.h"
#include "ViewGuiEngine.h"

WindowView::WindowView(DataPresenter* presenter)
{
	_presenter = presenter;
	_presenter->addObserver(this);

	_hInstance = GetModuleHandle(NULL);;

	_hWnd = CreateDialogParam(_hInstance, MAKEINTRESOURCE(IDD_MAINWINDOW), NULL, (DLGPROC)WndProc, reinterpret_cast<LPARAM>(this));

	if (!_hWnd)
	{
		//show error
	}
	ShowWindow(_hWnd, SW_SHOW);

	loadAccel();
}

void WindowView::update()
{
	int data = _presenter->getData();
	int resultData = _presenter->getResultData();
	const int bufSize = 20;
	WCHAR buf[bufSize];

	swprintf_s(buf, L"%d", data);
	SetDlgItemText(_hWnd, ID_EDIT, buf);

	swprintf_s(buf, L"%d", resultData);
	SetDlgItemText(_hWnd, ID_EDIT2, buf);
}

HACCEL WindowView::loadAccel()
{
	return LoadAccelerators(_hInstance, MAKEINTRESOURCE(IDC_WINDOWSPROJECT1));
}

//
//  WM_COMMAND  - ���������� ���� ����������
//  WM_PAINT    - ��������� �������� ����
//  WM_DESTROY  - ��������� ��������� � ������ � ���������
//
BOOL CALLBACK WindowView::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	WindowView* pThis;
	if (message == WM_INITDIALOG)
	{
		pThis = reinterpret_cast<WindowView*>(lParam);
		// Put the value in a safe place for future use
		SetWindowLongPtr(hWnd, DWLP_USER, reinterpret_cast<LONG_PTR>(pThis));
	}
	else
	{
		pThis = reinterpret_cast<WindowView*>(GetWindowLongPtr(hWnd, DWLP_USER));
	}

	switch (message)
	{
		case WM_COMMAND:
		{
			int wmId = LOWORD(wParam);
			// ��������� ����� � ����:
			switch (wmId)
			{
				case IDM_ABOUT:
					HINSTANCE hInstance;
					hInstance = GetModuleHandle(NULL);
					DialogBox(hInstance, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
					break;
				case IDM_EXIT:
					DestroyWindow(hWnd);
					break;
				case ID_BUTTON3:
				{
					const int bufSize = 20;
					WCHAR buf[bufSize];
					GetDlgItemText(hWnd, ID_EDIT, buf, bufSize);
					int data;
					int scanfResult = swscanf_s(buf, L"%d", &data);
					if (scanfResult != 0)
					{
						pThis->_presenter->setData(data);
					}
					else
					{
						//printError();
						//removeError();
					}
				}
				break;
				default:
					return DefWindowProc(hWnd, message, wParam, lParam);
			}
		}
		return TRUE;
		break;
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);
			
			EndPaint(hWnd, &ps);
		}
		return TRUE;
		break;
		case WM_DESTROY:
			PostQuitMessage(0);
			return TRUE;
			break;
		case WM_MOUSEFIRST:
			return TRUE;
			break;
	}

	return FALSE;
}

INT_PTR CALLBACK WindowView::About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

ViewEngine* WindowView::getEngine()
{
	return (ViewEngine*)(new ViewGuiEngine());
}