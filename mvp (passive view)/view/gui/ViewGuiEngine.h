#pragma once

#include "../ViewEngine.h"

class ViewGuiEngine: public ViewEngine
{
public:
	ViewGuiEngine();
	void run();
private:
	MSG _msg;
};

