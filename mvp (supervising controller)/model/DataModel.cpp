#include "DataModel.h"

DataModel::DataModel()
{

}

int DataModel::getData()
{
	return _data;
}

int DataModel::getResultData()
{
	return _resultData;
}

void DataModel::setData(int data)
{
	_data = data;

	calcResult();

	notifyUpdate();
}

void DataModel::calcResult()
{
	_resultData = _data * _data;
}
