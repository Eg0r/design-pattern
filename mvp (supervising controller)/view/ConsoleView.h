#pragma once

#include "../model/DataModel.h"
#include "../Observer.h"

#include <stdio.h>


class ConsoleView: public Observer
{
public:
	ConsoleView();
	void removeError();
	virtual void update() = 0;
};

