#pragma once

#include "../model/DataModel.h"
#include "../presenter/DataPresenter.h"
#include "ConsoleView.h"

class ConsoleSimpleView: public ConsoleView
{
public:
	ConsoleSimpleView(DataPresenter*);
	void update();
	void receive();
	void print(DataPresenter* presenter);
	void printWaitData();
	void printError();
	void updateData(DataModel*);
private:
	DataPresenter* _presenter;
};

