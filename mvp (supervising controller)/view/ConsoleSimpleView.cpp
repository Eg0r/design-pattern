#include "ConsoleSimpleView.h"

ConsoleSimpleView::ConsoleSimpleView(DataPresenter* presenter)
{
	_presenter = presenter;
	_presenter->addObserver(this);
}

void ConsoleSimpleView::update()
{
	print(_presenter);
}

void ConsoleSimpleView::receive()
{
	printWaitData();

	int data;
	int scanfResult = scanf_s("%d", &data);
	if (scanfResult != 0)
	{
		DataModel* model = _presenter->getModel();
		model->setData(data);
	}
	else
	{
		printError();
		removeError();
	}
}

void ConsoleSimpleView::print(DataPresenter* presenter)
{
	DataModel* model = presenter->getModel();
	printf("data: %d; result: %d\r\n\r\n", model->getData(), model->getResultData());
}

void ConsoleSimpleView::updateData(DataModel* model)
{
	int data;
	scanf_s("%d", &data);
	model->setData(data);
}

void ConsoleSimpleView::printWaitData()
{
	printf("enter digit for power of 2\r\n");
}

void ConsoleSimpleView::printError()
{
	printf("entered data not corrected\r\n\r\n");
}