#include "DataPresenter.h"

#include <stdio.h>

DataPresenter::DataPresenter(DataModel* model)
{
	_model = model;
	model->addObserver(this);
}

DataModel* DataPresenter::getModel()
{
	return _model;
}

void DataPresenter::update()
{
	notifyUpdate();
}
