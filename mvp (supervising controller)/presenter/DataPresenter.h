#pragma once

#include "../model/DataModel.h"
#include "../Observer.h"

class DataPresenter: public Observer, public Observable
{
public:
	DataPresenter(DataModel* model);
	void update();
	DataModel* getModel();
private:
	DataModel* _model;
};

