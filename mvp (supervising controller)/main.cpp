﻿#include <iostream>

#include "presenter/DataPresenter.h"
#include "./model/DataModel.h"
#include "./view/ConsoleSimpleView.h"

int main()
{
	DataModel* dataModel = new DataModel();
	DataPresenter* dataPresenter = new DataPresenter(dataModel);
	ConsoleSimpleView* view = new ConsoleSimpleView(dataPresenter);

	while (1)
	{
		view->receive();
	}
	//notify presenter for update model by ...

}