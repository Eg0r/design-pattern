#pragma once

#include <vector>

#include "Observer.h"

class Observable
{
public:
	void addObserver(Observer*);
	void notifyUpdate();
private:
	std::vector<Observer*> _observers;
};

