﻿#include <iostream>

#include "./model/DataModel.h"
#include "./controller/DataConsoleController.h"
#include "./view/ConsoleSimpleView.h"

int main()
{
	DataModel* dataModel = new DataModel();
	ConsoleSimpleView* view = new ConsoleSimpleView(dataModel);
	DataConsoleController* controller = new DataConsoleController(dataModel, view);

	while (1)
	{
		controller->receive();
	}
}