#pragma once

#include "../Observable.h"

class DataModel : public Observable
{
public:
	DataModel();
	int getData();
	int getResultData();
	void setData(int);
private:
	void calcResult();

	int _data;
	int _resultData;
};

