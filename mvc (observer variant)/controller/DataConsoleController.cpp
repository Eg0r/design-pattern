#include "DataConsoleController.h"

#include <stdio.h>

DataConsoleController::DataConsoleController(DataModel* model, ConsoleView* view)
{
	_model = model;
	_view = view;
}

void DataConsoleController::setData(int data)
{
	_model->setData(data);
}

void DataConsoleController::receive()
{
	_view->printWaitData();

	int data;
	int scanfResult = scanf_s("%d", &data);
	
	if (scanfResult != 0)
	{
		setData(data);
	}
	else
	{
		showError();
		removeError();
	}
}

void DataConsoleController::showError()
{
	_view->printError();
}


