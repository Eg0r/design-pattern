#pragma once

#include "../model/DataModel.h"
#include "../view/ConsoleView.h"
#include "../controller/ConsoleController.h"

class DataConsoleController: public ConsoleController
{
public:
	DataConsoleController(DataModel* model, ConsoleView* view);
	void setData(int data);
	void receive();
	void showError();
private:
	DataModel* _model;
	ConsoleView* _view;
};

