#pragma once

#include "../model/DataModel.h"
#include "../Observer.h"

#include <stdio.h>


class ConsoleView: public Observer
{
public:
	ConsoleView();
	virtual void update() = 0;
	virtual void printWaitData() = 0;
	virtual void printError() = 0;
protected:
	DataModel* _model;
};

