#include "ConsoleSimpleView.h"

ConsoleSimpleView::ConsoleSimpleView(DataModel* model)
{
	_model = model;
	_model->addObserver(this);
}

void ConsoleSimpleView::printWaitData()
{
	printf("enter digit for power of 2\r\n");
}

void ConsoleSimpleView::printError()
{
	printf("entered data not corrected\r\n\r\n");
}

void ConsoleSimpleView::update()
{
	printf("data: %d; result: %d\r\n\r\n", _model->getData(), _model->getResultData());
}