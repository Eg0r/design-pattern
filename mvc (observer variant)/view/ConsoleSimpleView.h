#pragma once

#include "../model/DataModel.h"
#include "ConsoleView.h"

class ConsoleSimpleView: public ConsoleView
{
public:
	ConsoleSimpleView(DataModel*);
	void printError();
	void printWaitData();
	void update();
};

