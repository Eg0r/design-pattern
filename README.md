# Design Pattern

This C++ project is template for some design patterns: MVC, MVP passive view, MVP supervising controller. This is simple demonstration of how to use design patterns.
Implementation in this project it is possible to simultaneously work with application in the console and in the window.


Views in this project:


  * Console View
  * Window View (MVP passive view only, other patterns later)
  * Http View (later)
  

### Sources of information for implementations patterns
  * https://habr.com/ru/post/50830/
  * https://habr.com/ru/post/53536/
  * https://habr.com/ru/post/53920/
  * https://habr.com/ru/post/53943/
  * https://martinfowler.com/eaaDev/SupervisingPresenter.html
  * https://martinfowler.com/eaaDev/PassiveScreen.html
  * https://habr.com/ru/company/mobileup/blog/313538/

### Todos
 * add window view for all patterns
 * implementate http view